# jsTree menu

This module allows you to render all your menus with jsTree library via blocks

 - Once you have the module enabled you will have a new block called "jsTree
   menu". On each instance of this block type you will be able to select any
   of the existing menus. Then, every instance of this block will then render
   its menu with jsTree.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/jstree_menu).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/jstree_menu).


## Table of contents

 - Requirements
 - Installation
 - Configuration
 - Troubleshooting
 - Maintainers
 - Links


## Requirements

This module requires no modules outside of Drupal core. However, it requires to
install the jstree library (see installation section).


## Installation

 - In Drupal 8 version of this module "jquery_jstree" is no longer required.

 - For Drupal 9 compatibility, the Libraries API module is no longer required.
   However, for backwards compatibility, the folder where the jstree libraries
   should be set remains the same.

 - Install the [jstree libraries](https://www.jstree.com/) into
   DRUPAL_ROOT/libraries/jstree
   You need to put there the whole content of "dist" directory inside jstree
   ZIP file. Once done, you should have the file
   DRUPAL_ROOT/libraries/jstree/jstree.min.js
   and all jstree themes under
   DRUPAL_ROOT/libraries/jstree/themes/*

 - [OPTIONAL] Install the "proton" theme for jsTree into libraries folder. Go to
   [jstree bootstrap theme](https://github.com/orangehill/jstree-bootstrap-theme)
   and download it.

   Copy the `dist/themes/proton` folder to `DRUPAL_ROOT/libraries/jstree/themes`

   If done right, you will have style.min.css file on:
   `DRUPAL_ROOT/libraries/jstree/themes/proton/style.min.css`

 - Install as you would normally install a contributed Drupal module. For
   further information, see
   [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

 - To render the tree icons correctly you will need to install the
    [Bootstrap theme](https://www.drupal.org/project/bootstrap) or
    [Bootstrap library](https://www.drupal.org/project/bootstrap_library)
    (default icons) or the [Font Awesome module](https://www.drupal.org/project/fontawesome)


## Configuration

 - Configure module's options in Administration » Configuration » User Interface
   » jsTree menu:

    - jsTree theme

      Allows you to choose between "default" jsTree theme or "proton" (which has
      to be downloaded)

    - Menu maximum height

      Allows you to choose the height of the menu.

    - Remove border of jsTree

      Allows to remove the border of the tree.

    - Normal icon

      Icon displayed on every node of tree except leaves. You can use Bootstrap
      glyphicons and/or Font Awesome icons.

      - Bootstrap glyphicons example: glyphicon glyphicon-tag. See more at
      [Components](http://getbootstrap.com/components/)
      - Font Awesome example: fa fa-file. see more at [Font awesome icons](http://fontawesome.io/icons/)

    - Leaves icon

      Icon displayed on tree leaves. You can use Bootstrap glyphicons and/or
      Font Awesome icons.

      - Bootstrap glyphicons example: glyphicon glyphicon-tag. See more at
      [Components](http://getbootstrap.com/components/)
      - Font Awesome example: fa fa-file . see more at [Font awesome icons](http://fontawesome.io/icons/)


## Troubleshooting

 - If you only see level 1 items on the menu just edit parent menu items and
   mark their checkbox "Show as expanded" and save the changes.


## Maintainers

- Roger Codina - [rcodina](https://www.drupal.org/u/rcodina)


### LINKS

 - (https://www.jstree.com/)
 - (https://github.com/vakata/jstree)
 - (https://github.com/orangehill/jstree-bootstrap-theme)
 - (https://www.orangehilldev.com/jstree-bootstrap-theme/demo/)
