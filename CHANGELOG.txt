jsTree menu 2.1.1, 2024-08-30
-----------------------------
#3321878: Switch to README.md
#3470571: Enable gitlab CI and fix pipeline errors

jsTree menu 2.1.0, 2024-08-17
-------------------------------
#3431495: Compatibility with Drupal 11
#3362863: Use better semantics in the HTML container
#3362852: Missing administration menu link

jsTree menu 2.0.0, 2022-08-24
-------------------------------
Compatibility with Drupal 10

jsTree menu 8.x-1.1, 2020-08-29
-------------------------------
#3164441: Unable to install in Drupal 9

jsTree menu 8.x-1.0, 2020-05-12
-------------------------------
Fix error when the selected menu is empty
Improve source code using PHPCodeSniffer
#3128917: All dependencies must be prefixed with the project name, for example "drupal:"

jsTree menu 8.x-1.0-rc1, 2020-04-14
-----------------------------------
Full compatibility with Drupal 9
Requires Drupal 8.7.7 or newer
README improvements
