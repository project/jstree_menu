<?php

namespace Drupal\jstree_menu\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure order for this site.
 */
class JsTreeMenuForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jstree_menu_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'jstree_menu.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('jstree_menu.config');

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General settings'),
      '#weight' => 5,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['general']['jstree_menu_theme'] = [
      '#type' => 'select',
      '#title' => $this->t('jsTree theme'),
      '#options' => [
        'default' => $this->t('Default'),
        'proton' => $this->t('Proton'),
      ],
      '#default_value' => $config->get('jstree_menu_theme'),
      '#description' => $this->t('You may have to clear the cache for changes to take effect'),
    ];

    $form['general']['jstree_menu_height'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu maximum height'),
      '#options' => [
        'auto' => $this->t('Automatic'),
        '300px' => $this->t('300px'),
        '500px' => $this->t('500px'),
      ],
      '#default_value' => $config->get('jstree_menu_height'),
      '#description' => $this->t('If you select automatic there will be no vertical scroll and all contents will be visible'),
    ];

    $form['general']['jstree_menu_remove_border'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove border of jsTree.'),
      '#default_value' => $config->get('jstree_menu_remove_border'),
    ];

    $form['icons'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Icon settings'),
      '#weight' => 6,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['icons']['desc'] = [
      '#type' => 'item',
      '#title' => $this->t('Help'),
      '#markup' => $this->t('You can use Bootstrap glyphicons and/or Font Awesome icons (see README file for more details).'),
    ];

    $form['icons']['jstree_menu_icon'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Normal icon'),
      '#required' => TRUE,
      '#default_value' => $config->get('jstree_menu_icon'),
      '#description' => $this->t('Icon displayed on every node of tree except leaves.'),
    ];

    $form['icons']['jstree_menu_icon_leaves'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Leaves icon'),
      '#required' => TRUE,
      '#default_value' => $config->get('jstree_menu_icon_leaves'),
      '#description' => $this->t('Icon displayed on tree leaves'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('jstree_menu.config')
      ->set('jstree_menu_theme', $values['jstree_menu_theme'])
      ->save();

    $this->config('jstree_menu.config')
      ->set('jstree_menu_height', $values['jstree_menu_height'])
      ->save();

    $this->config('jstree_menu.config')
      ->set('jstree_menu_remove_border', $values['jstree_menu_remove_border'])
      ->save();

    $this->config('jstree_menu.config')
      ->set('jstree_menu_icon', $values['jstree_menu_icon'])
      ->save();

    $this->config('jstree_menu.config')
      ->set('jstree_menu_icon_leaves', $values['jstree_menu_icon_leaves'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
