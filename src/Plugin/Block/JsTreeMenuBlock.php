<?php

namespace Drupal\jstree_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\system\Entity\Menu;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides jstree menu block.
 *
 * @Block(
 *   id = "js_tree_menu_block",
 *   admin_label = @Translation("jsTree menu"),
 *   category = @Translation("Blocks")
 * )
 */
class JsTreeMenuBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Menu link tree service.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Constructs a JsTreeMenuBlock block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menuLinkTree
   *   Menu tree service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Configuration factory.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    MenuLinkTreeInterface $menuLinkTree,
    ConfigFactory $configFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuLinkTree = $menuLinkTree;
    $this->config = $configFactory->get('jstree_menu.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block_config = $this->getConfiguration();

    $level = 4;
    $menu_name = !empty($block_config['menu']) ? $block_config['menu'] : 'main';

    $parameters = $this->menuLinkTree->getCurrentRouteMenuTreeParameters($menu_name);
    $parameters->setMaxDepth($level + 1);
    $tree = $this->menuLinkTree->load($menu_name, $parameters);

    if (is_array($tree) && count($tree) > 0) {

      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ];
      $tree = $this->menuLinkTree->transform($tree, $manipulators);
      $menu = $this->menuLinkTree->build($tree);

      $theme = $this->config->get('jstree_menu_theme');
      if ($theme == 'default') {
        $libraries = ['jstree_menu/jstree', 'jstree_menu/jstree_menu'];
      }
      else {
        $libraries = ['jstree_menu/jstree', 'jstree_menu/jstree_proton', 'jstree_menu/jstree_menu'];
      }

      // Call to custom twig template.
      return [
        '#theme' => 'jstree_menu_menu',
        '#menu_name' => $menu['#menu_name'],
        '#items' => $menu['#items'],
        '#id' => $menu['#menu_name'],
        '#icon' => $this->config->get('jstree_menu_icon'),
        '#icon_leaves' => $this->config->get('jstree_menu_icon_leaves'),
        '#attached' => [
          'library' => $libraries,
          // Pass variables to JS.
          'drupalSettings' => [
            'jstree_menu' => [
              'theme' => $theme,
              'rem_border' => $this->config->get('jstree_menu_remove_border'),
              'height' => $this->config->get('jstree_menu_height'),
            ],
          ],
        ],
        // https://www.drupal.org/docs/8/api/cache-api/cache-contexts
        '#cache' => [
          'contexts' => ['url.path'],
        ],
      ];

    }
    else {
      $markup = '<p>' . $this->t('Please, make sure @menu menu has links.', ['@menu' => $menu_name]) . '</p>';
    }

    $build = [
      '#type' => 'markup',
      '#markup' => $markup,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $menu_options = Menu::loadMultiple();
    foreach ($menu_options as $menu_name => $menu) {
      $menu_options[$menu_name] = $menu->label();
    }
    asort($menu_options);

    $form['js_tree_menu_block_menu'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu'),
      '#options' => $menu_options,
      '#description' => $this->t('Select the menu you want to render with jsTree library'),
      '#default_value' => $config['menu'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['menu'] = $form_state->getValue('js_tree_menu_block_menu');
  }

}
